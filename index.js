const express = require('express')
const mongoose = require('mongoose')
const cors = require('cors');
const dotenv = require('dotenv').config();

const PORT = process.env.PORT || 4028;
const app = express();

// ROUTE MODULE

const userRoutes = require(`./routes/userRoutes`);
const productRoutes = require(`./routes/productRoutes`);
const cartRoutes = require(`./routes/cartRoutes`);
const orderRoutes = require(`./routes/orderRoutes`);


// MIDDLEWARES

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())

//MONGOOSE CONNECTION

mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});

// DB CONNECTION NOTIFICATION

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log(`Connected to database`));


//ROUTES
const path = require('path');

app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productRoutes);
app.use(`/api/carts`, cartRoutes);
app.use(`/api/orders`, orderRoutes);



app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))
