const express = require('express');
const router = express.Router();
const {verify,decode,verifyAdmin} = require('./../auth')

const {cartList,
        create,
        getList,
        archive,
        unarchive,
        deleteCart,
        cart,
        editQty} = require('./../controllers/cartControllers');


//GET ALL CARTS

router.get(`/allList`, verifyAdmin, async(req, res) => {   
    try{
        await cartList().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})

//CREATE CARTS

router.post(`/create`, verify, async(req,res) => {  

    const userId =  decode(req.headers.authorization).id

    try{
        await create(userId, req.body).then(result => res.send(result))
        
    }catch(err){
        res.status(500).json(err)
    }
})

//FIND USER'S CARTS LIST

router.get(`/myCarts`, verify, async(req,res) => {

    const userId = decode(req.headers.authorization).id
    
    try{
        await getList(userId).then(result => res.send(result))
        
    }catch(err){
        res.status(500).json(err)
    }
})

//GET AN SPECIFIC CART

router.get(`/:cartId`, verify, async(req,res)  => {

    const cartId = req.params.cartId;

    try{
        await cart(cartId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 

})

//EDIT QUANTITY

router.patch(`/:cartId/editQty`, verify, async(req,res) => {

    const cartId = req.params.cartId;
  
    try{
        await editQty(cartId, req.body).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//ARCHIVE CART

router.patch(`/:cartId/archive`, verify, async(req,res) => {

    const cartId = req.params.cartId;
    
    try{
        await archive(cartId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//UNARCHIVE CART

router.patch(`/:cartId/unarchive`, verify, async(req,res) => {

    const cartId = req.params.cartId;
    
    try{
        await unarchive(cartId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//DELETE CART

router.delete('/:cartId/delete', verify, async (req, res) => {

    const cartId = req.params.cartId;
    try{
        await deleteCart(cartId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

//ROUTER
module.exports = router;

