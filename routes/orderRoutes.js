const { Router } = require('express');
const express = require('express');
const router = express.Router();
const {verify,decode,verifyAdmin} = require('../auth')

const {getList,
    createOrder,
    myOrder,
    order,
    archiveOrder,
    unarchiveOrder,
    deleteOrder} = require('../controllers/orderControllers');


//GET ALL ORDERS

router.get(`/allOrders`, verifyAdmin, async(req, res) => {   
    try{
        await getList().then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 
})

//CREATE ORDER

router.post(`/create`, verify, async(req,res) => {  

    const userId =  decode(req.headers.authorization).id

    try{
        await createOrder(userId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }

})


//FIND USER'S ORDER LIST

router.get(`/myOrder`, verify, async(req,res) => {


    const userId = decode(req.headers.authorization).id

    
    try{
        await myOrder(userId).then(result => res.send(result))
        
    }catch(err){
        res.status(500).json(err)
    }
})

//GET AN SPECIFIC ORDER

router.get(`/:orderId`, verify, async(req,res)  => {

    const orderId = req.params.orderId;

    try{
        await order(orderId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    } 

})

//ARCHIVE ORDER

router.patch(`/:orderId/archive`, verify, async(req,res) => {

    const orderId = req.params.orderId;
    
    try{
        await archiveOrder(orderId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

//UNARCHIVE ORDER

router.patch(`/:orderId/unarchive`, verify, async(req,res) => {

    const orderId = req.params.orderId;
    
    try{
        await unarchiveOrder(orderId).then(result => res.send(result))
    }catch(err){
        res.status(500).json(err)
    }
})

router.delete('/:orderId/delete', verify, async (req, res) => {

    const orderId = req.params.orderId;
    try{
        await deleteOrder(orderId).then(result => res.send(result))

    }catch(err){
        res.status(500).json(err)
    }
})

//ROUTER
module.exports = router;

