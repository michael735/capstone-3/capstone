const Cart = require('./../models/Cart');
const Product = require('./../models/Product');

//GET CART LIST

module.exports.cartList = async() => {
    
    return await Cart.find().then(result => result)
}

//CREATE CART
module.exports.create = async(userId, reqBody) => {

    const {productId,quantity} = reqBody

    const product = await Product.findById(productId)
    const price = product.price
    const description = product.description


    const newCart = new Cart ({   
        userId: userId,
        productId: reqBody.productId,
        description: description,
        price: price,
        quantity: reqBody.quantity,
        bill: (reqBody.quantity*price)    
    })

    const prodQuantity = product.quantity
    const newQuantity = (prodQuantity - newCart.quantity)

    return await newCart.save().then(result => {
    
		if(result){  
            if (newQuantity < 0){
                return `No stocks left`
            }else if (newQuantity === 0){
                Product.findByIdAndUpdate(productId, {$set:{quantity: newQuantity, IsActive: false}}).then((res,err) => res ? true : err) 
                return true
            }
            else{
                Product.findByIdAndUpdate(productId, {$set:{quantity: newQuantity }}).then((res,err) => res ? true : err) 
                return true
            }	
		} else { 
			if(result == null){
				return false
			}
		}
	})
}

//GET USER'S CART LIST

module.exports.getList = async(id) => {
   
    return await Cart.find({"userId": id, "isActive" : false}).then(result => result)
}

//GET AN SPECIFIC CART
module.exports.cart = async (id) => {

	return await Cart.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Cart does not exist`}
			} else {
				return err
			}
		}
	})
}

//EDIT CART QTY

module.exports.editQty = async (id, reqBody) => {

    
    const newQty = ({
                    quantity : reqBody.quantity,
                    price : reqBody.price,
                    bill : (reqBody.quantity*reqBody.price)
                    })

	return await Cart.findByIdAndUpdate(id, {$set:newQty}, {new:true}).then((result, err) => {
		if(result){
            return result
		}
        else{
			if(result == null){
				return {message: `Cart does not exist`}
			} else {
				return err
			}
		}
	})

}
//ARCHIVE CART

module.exports.archiveCart= async(id) => {

	return await Cart.findByIdAndUpdate(id, {$set:{isActive: false}}, {new:true}).then ((result,err) => result ? result : err)
}

//UNARCHIVE CART

module.exports.unarchive= async(id) => {

	return await Cart.findByIdAndUpdate(id, {$set:{isActive: true}}, {new:true}).then ((result,err) => result ? result : err)
}

//DELETE CART

module.exports.deleteCart = async(id) => {

	return await Cart.findByIdAndDelete(id).then ((result,err) => result ? true : err)
}
