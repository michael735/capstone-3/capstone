const { stringify } = require('nodemon/lib/utils');
const Cart = require('./../models/Cart');
const Order = require('./../models/Order');



//GET ORDER LIST

module.exports.getList  = async() => {
    
    return await Order.find().then(result => result)
}


//CREATE ORDER

module.exports.createOrder = async(id) => {
    const order = await Order.find({"userId": id , "isActive": false})

    const bills = order.map(result => result.bill)

    const totalBill = bills.reduce((acc, cur) => acc + cur)

    const newOrder = new Order ({
        userId: id,
        orders: order,
        totalBill: totalBill
    })

    return await newOrder.save().then(result => {
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}

//GET USER'S ORDER

module.exports.myOrder= async(id) => {
   
    return await Order.find({"userId": id}).then(result => result)
}


//GET AN SPECIFIC ORDER
module.exports.order = async (id) => {

	return await Order.findById(id).then((result, err) => {
		if(result){
			return result
		}else{
			if(result == null){
				return {message: `Order does not exist`}
			} else {
				return err
			}
		}
	})
}
 
//ARCHIVE ORDER

module.exports.archiveOrder= async(id) => {

	return await Order.findByIdAndUpdate(id, {$set:{active: false}}, {new:true}).then ((result,err) => result ? result : err)
}

//UNARCHIVE ORDER

module.exports.unarchiveOrder= async(id) => {

	return await Order.findByIdAndUpdate(id, {$set:{active: true}}, {new:true}).then ((result,err) => result ? result : err)
}

//DELETE ORDER

module.exports.deleteOrder = async(id) => {

	return await Order.findByIdAndDelete(id).then ((result,err) => result ? true : err)
}
