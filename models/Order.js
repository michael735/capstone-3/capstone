const mongoose = require("mongoose");
const cartSchema = require(`./Cart`)

const OrderSchema = new mongoose.Schema({

  userId:{
    type: mongoose.Schema.Types.ObjectId,
    ref: `User`,
    required: true
  },
  cartItems:[{
    cartId:{ 
        type: mongoose.Schema.Types.ObjectId,
        ref: `Cart`,
        required: [true, `Cart to be ordered is required`]},
    productId:{
        type: mongoose.Schema.Types.ObjectId,
        ref: `Product`,
        required: [true, `Product to be ordered is required`]
    },
    description:{
        type: String
    },
    price:{
        type: Number,
        default: 1,
        min: 1
    },
    quantity:{
        type: Number,
        default: 1,
        min: 1
    },
    bill:{
        type: Number,
        default: 1,
        min: 1
    },
    purchasedOn:{
        type: Date,
        default: Date.now
    },
    isActive:{
        type: Boolean,
        default: true
    }
}],
  active: {
    type: Boolean,
    default: true
  },
  totalBill:{
    type: Number,
    default: 1
  },
  modifiedOn: {
    type: Date,
    default: Date.now
  }
},
{timestamps: true})

module.exports = mongoose.model("Order", OrderSchema);